# RCP design documentation

This repository holds documents describing the DSA-2000 RCP subsystem
design.

On gitlab.com a tag is generated for the repository, and rendered
versions of the document are generated, whenever the "release" CI job
is manually started. That job will create a tag based on the last
version number in the list of versions in the `01-frontmatter.md`
file; the purpose being to maintain consistency in document and git
repository version numbers. Multiple rendered versions of the document
are created by the CI pipeline:
* on the
  [wiki](https://gitlab.com/dsa-2000/planning/rcp/docs/design/-/wikis/design),
  and
* in a versioned package in the [package
  repository](https://gitlab.com/dsa-2000/planning/rcp/docs/design/-/packages).

## Instructions for generating PDF version of documentation

For those wanting to generate a PDF locally...some examples:

### Interactive version

``` shell
pandoc docs/*.md -f markdown+pipe_tables -o design.pdf -s \
 -V colorlinks \
 -V logo=docs/templates/pandoc/d2k-logo.png --number-sections \
 --template docs/templates/pandoc/template.latex
```

### Print version

``` shell
pandoc docs/*.md -f markdow+pipe_tables -o design.pdf -s \
 -V colorlinks -V links-as-notes \
 -V logo=docs/templates/pandoc/d2k-logo.png --number-sections \
 --template docs/templates/pandoc/template.latex
```
