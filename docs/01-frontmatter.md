---
title: RCP computing hardware design
author:
- Martin Pokorny
abstract: |
  We describe the design of the DSA-2000 RCP subsystem computing
  hardware deployment.
document-number: 00051
wbs-level-2-abbrev: RCP
document-type-abbrev: DES
revisions:
- version: 1
  date: 2023-10-20
  remarks: Initial version
  authors:
  - Martin Pokorny
- version: 2
  date: 2023-10-22
  remarks: Update RCP-L upper frequency limit
  sections:
  - rcp-imaging-nodes
  - rcp-h-instance-nodes
  - rcp-l-instance-nodes
  authors:
  - Martin Pokorny
...
