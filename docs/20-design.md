# Scope

The RCP subsystem hardware comprises a collection of networked
computers that host processes for DSA-2000 imaging, pulsar and fast
transient detection, and some scientific image processing. All of the
computers in the RCP subsystem have network connections to computers
in the radio camera front-end (RCF) and data management (DAT)
subsystems, which provide the interfaces over which the data inputs
and outputs flow.

# RCP imaging nodes {#rcp-imaging-nodes}

The imaging processes for DSA-2000 are classified according the set of
input channels received by each process, and the image types that are
produced by each process. The four classes, or instances, are defined
as follows:

- RCP-H: receives dual-polarization NC channels from RCF above 1.42912
  GHz, and produces MP images in that frequency interval;
- RCP-L: receives dual-polarization NC channels from RCF below 1.42912
  GHz, and produces both MP and NL images in that frequency interval;
- RCP-A: receives dual-polarization AC channels from RCF, and produces
  ZA images across that frequency interval;
- RCP-B: receives dual-polarization BC channels from RCF, and produces
  ZB images across that frequency interval.

Processes in each of the RCP instances execute on nodes with a
particular configuration, depending on the instance to which they
belong.

All nodes for RCP imaging are provisioned with a 200Gb Ethernet
interface to the RCF network to receive input data, and to connect
with DAT systems. A 1 Gb Ethernet interface is used for the interface
with the MC subsystem.

All nodes for RCP imaging are provisioned with 10x NVIDIA RTX 4000 Ada
generation GPUs. The allocation of GPUs to input channels and image
products differs according to the RCP instance, as described below.

Memory usage on GPUs is largely determined by the number of image
grids, voltage sample sets, and visibility sets that a GPU maintains
in memory.

GPU memory usage (assuming four dual-polarization input channels
processed per GPU)

- image grid: at 16K x 16K pixels, with double-precision floating
  point complex values, and only the central portion of the grid held
  in GPU memory (assume 6800 x 6800 pixels), each grid requires about
  700 MiB of memory
- voltage samples and flags: voltage samples and flags for a
  single cross-correlation interval, about 3 GiB in RCP-L and RCP-H
  (much less in RCP-A and RCP-B)
- visibilities: visibilities for a single timestamp value, about 0.25
  GiB

CPU memory usage (assuming 40 dual-polarization input channels
processed per server)

- image grid: at 16K x 16K pixels, with double-precision floating
  point complex values, each grid requires about 4 GiB
- voltage samples and flags: 30 GiB
- calibration: 200 GiB

## RCP-H instance nodes {#rcp-h-instance-nodes}

| Server configuration    |                                 |
|-------------------------+---------------------------------|
| Chassis                 | 4U                              |
| CPUs                    | 1x Xeon 6430 32 core, 2x2.1 GHz |
| RAM                     | 1 TB                            |
| System disk             | 480 GB NVMe                     |
| Data disk               | 4 GB (usable) SSD, Raid 1       |
| Input network interface | 2x 100 GbE                      |
| MC network              | 1 GbE                           |
| GPUs                    | 10x NVIDIA RTX 4000 Ada         |

Input data rate, per dual-polarization channel: 4.2 Gbps.

4 RCF dual-polarization channels, 4 MP grids per GPU. 20 GiB memory is
sufficient (with double buffering, minimum).

6 CPU threads per GPU.

Task summary:

- Imaging (RCP)
  - data capture, cross-correlation, flagging, calibration solution,
    calibration application, gridding, image FFT.
- Post-processing (DAT)
  - MP images: dewarp solution, dewarp application, mosaic.

| Node count                                                   |      |
|--------------------------------------------------------------+------|
| Total number dual-polarization NC channels above 1.42912 GHz | 4400 |
| Number dual-polarization NC channels per GPU                 |    4 |
| Number GPUs per node                                         |   10 |
| Total nodes                                                  |  110 |

## RCP-L instance nodes {#rcp-l-instance-nodes}

| Server configuration    |                                 |
|-------------------------+---------------------------------|
| Chassis                 | 4U                              |
| CPUs                    | 1x Xeon 6430 32 core, 2x2.1 GHz |
| RAM                     | 1 TB                            |
| System disk             | 480 GB NVMe                     |
| Data disk               | 15 TB (usable) SAS, Raid 1      |
| Input network interface | 2x 100 GbE                      |
| MC network              | 1 GbE                           |
| GPUs                    | 10x NVIDIA RTX 4000 Ada         |

Input data rate, per dual-polarization channel: 4.2 Gbps.

4 RCF dual-polarization channels, 4 NL grids, 4 MP grids per GPU. 20
GiB memory is sufficient (with double buffering, minimum).

6 CPU threads per GPU.

Task summary:

- Imaging (RCP)
  - data capture, cross-correlation, flagging, calibration solution,
    calibration application, gridding, image FFT.
- Post-processing (DAT)
  - MP images: dewarp solution, dewarp application, mosaic
  - NL images: dewarp application, mosaic.


| Node count                                                   |      |
|--------------------------------------------------------------+------|
| Total number NC dual-polarization channels below 1.42912 GHz | 5600 |
| Number dual-polarization NC channels per GPU                 |    4 |
| Number GPUs per node                                         |   10 |
| Total nodes                                                  |  140 |

## RCP-A and RCP-B instance nodes

| Server configuration    |                                  |
|-------------------------+----------------------------------|
| Chassis                 | 4U                               |
| CPUs                    | 2x Xeon 4416+ 20 core, 2x2.0 GHz |
| RAM                     | 1 TB                             |
| System disk             | 480 GB NVMe                      |
| Data disk               | 15 TB (usable) SAS, Raid 1       |
| Input network interface | 2x 100 GbE                       |
| MC network              | 1 GbE                            |
| GPUs                    | 10x NVIDIA RTX 4000 Ada          |

Input data rate, per dual-polarization channel

- RCP-A: 0.26 Gbps
- RCP-B: 0.03 Gbps

4 RCF dual-polarization channels, 4 NL grids per GPU. Each GPU can
process a contiguous interval of four dual-polarization channels in
either AC or BC.

4 CPU threads per GPU.

Task summary:

- Imaging (RCP)
  - data capture, cross-correlation, flagging, calibration solution,
    calibration application, gridding, image FFT.
- Post-processing (DAT)
  - NL images: dewarp application, mosaic.

| Node count                                 |      |
|--------------------------------------------+-----:|
| Total number dual-polarization AC channels | 4192 |
| Total number dual-polarization BC channels |  960 |
| Number dual-polarization channels per GPU  |    4 |
| Number GPUs per node                       |   10 |
| Total nodes                                |  129 |

# FTD nodes

All nodes for FTD processing are provisioned with a 200Gb Ethernet
interface to the RCF network to receive input data, and to connect
with DAT systems. Additionally, these nodes are connected to a 400Gb
Ethernet interface to provide the high bandwidth fabric by which the
channel-beam data transpose is implemented.

FTD nodes are provisioned with 8x NVIDIA RTX 4000 Ada generation
GPUs.

| Server configuration          |                                    |
|-------------------------------+------------------------------------|
| Chassis                       | 4U                                 |
| CPUs                          | 2x AMD EPYC 9224 24 core, 2x2.5GHz |
| RAM                           | 2.3 TB                             |
| Disk                          | 2 TB SSD                           |
| Input network interface       | 2x 100 GbE                         |
| Corner turn network interface | 2x 200 GbE                         |
| MC network                    | 1GbE                               |
| GPUs                          | 8x NVIDIA RTX 4000 Ada             |

Input data rate, per dual-polarization channel: 4.2 Gbps.

Task summary:

  - timing
  - search.

Number of nodes: 150 (see document D2k-00018-RCP-PSRFRB for details)
